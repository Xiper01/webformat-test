from django.contrib.auth.models import AbstractUser
from django.db import models
from enum import IntEnum

# Refer to https://docs.djangoproject.com/en/4.0/ref/models/fields/#enumeration-types
# for the usage of enums

# https://dev.to/bencleary89/using-enums-as-django-model-choices-4dk5

class Employee(AbstractUser):
  class Role(IntEnum):
    CEO = 1
    PM = 2
    DEV = 3

    @classmethod
    def choices(cls):
      return [(key.value, key.name) for key in cls]
  role = models.IntegerField(choices=Role.choices(), default=Role.DEV)

  def get_role_name(self):
    return self.Role(self.role).name.title()

  def __str__(self):
    return f"{self.get_role_name()} {self.first_name} {self.last_name}"