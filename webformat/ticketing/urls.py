from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('logout', views.logout, name='logout'),
    path('teams', views.get_teams, name='get_teams'),
    path('teams/<int:pk>', views.get_team, name='get_team'),
    path('teams/<int:pk>/edit', views.edit_team, name='edit_team'),
    path('teams/<int:pk>/delete', views.delete_team, name='delete_team'),
    path('teams/new', views.new_team, name='new_team'),

    path('projects', views.get_projects, name='get_projects'),
    path('projects/<int:pk>', views.get_project, name='get_project'),
    path('projects/<int:pk>/create_task', views.create_task, name='create_task'),
    path('projects/<int:pk>/delete_task', views.delete_task, name='delete_task'),
    path('projects/<int:pk>/edit', views.edit_project, name='edit_project'),
    path('projects/<int:pk>/delete', views.delete_project, name='delete_project'),
    path('projects/new', views.new_project, name='new_project'),

    path('tasks/', views.get_tasks, name='get_tasks'),
    path('tasks/<int:pk>', views.update_task, name='update_task'),
    path('tasks/<int:pk>/commits', views.commits_task, name='commits_task'),

    path('employees', views.get_employees, name='get_employees'),
    path('employees/<int:pk>', views.get_employee, name='get_employee'),
]