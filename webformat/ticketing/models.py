from msilib.schema import Registry
from pyexpat import model
from statistics import mode
from django.db import models
from django.conf import settings
from django.urls import reverse
from enum import IntEnum

# Create your models here.

class Team(models.Model):
  members = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='teams')
  name = models.CharField(max_length=30)
  description = models.TextField(null=True)

  def __string__(self):
    return self.name

  def get_absolute_url(self):
    return reverse("nomeRoot", kwargs={"pk": self.pk})


class Project(models.Model):
  creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
  pm = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='pm')
  name = models.CharField(max_length=30)
  description = models.TextField(null=True)

  def __string__(self):
    return self.name

  def get_absolute_url(self):
    return reverse("nomeRoot", kwargs={"pk": self.pk})


class Task(models.Model):

  class Status(IntEnum):
    TO_DO = 1
    IN_PROGRESS = 2
    STAGE = 3
    DONE = 4

    @classmethod
    def choices(cls):
      return [(key.value, key.name) for key in cls]

  description = models.CharField(max_length=30)
  status = models.IntegerField(choices=Status.choices(), default=Status.TO_DO)
  deadline = models.DateField()
  creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
  project = models.ForeignKey(Project, on_delete=models.CASCADE)
  assignees = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='tasks')

  @property
  def get_status_name(self):
    return self.Status(self.status).name.title()
  
  def __string__(self):
    return self.description

  def get_absolute_url(self):
    return reverse("nomeRoot", kwargs={"pk": self.pk})


class Commit(models.Model):
  creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
  task = models.ForeignKey(Task, on_delete=models.CASCADE)
  note = models.TextField()

  def __string__(self):
    return self.note

  def get_absolute_url(self):
    return reverse("nomeRoot", kwargs={"pk": self.pk})

