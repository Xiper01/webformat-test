from ast import For
from hashlib import new
from multiprocessing import context
from pydoc import describe
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

from ticketing.models import Team
from django.conf import settings
from django.contrib.auth import get_user_model
from ticketing.forms import FormCreateTeam

# GET all teams
def get_teams(request):
  context = {"teams": Team.objects.all()}
  return render(request, "team/index.html", context)

# GET specific team
def get_team(request, pk):
  team = Team.objects.get(pk=pk)
  context = {
    "team": team,
    "logged_user": request.user
  }
  return render(request, "team/show.html", context)

# GET create a new team
def new_team(request):
  if request.method == "POST":
    form = request.POST
    new_team = Team(name = form.get("name"), description = form.get("description"))
    new_team.save()
    new_team.members.set(form.getlist("developer"))
    return redirect(get_teams)
  else:
    form = FormCreateTeam()
    developers = get_user_model().objects.filter(role = 3)
  context = {
    "form": form,
    "developers": developers
  }
  return render(request, "team/new.html", context)

# GET edit team
def edit_team(request, pk):
  team = Team.objects.get(pk=pk)
  if request.method == "POST":
    form = request.POST
    team.name = form.get("name")
    team.description = form.get("description")
    team.save()
    team.members.set(form.getlist("developer"))
    return redirect(get_teams)
  else:
    form = FormCreateTeam()
    developers = get_user_model().objects.filter(role = 3)
  context = {
    "form": form,
    "developers": developers,
    "team": team
  }
  return render(request, "team/edit.html", context)

# POST delete team
def delete_team(request, pk):
  team = Team.objects.get(pk=pk)
  team.delete()
  return redirect(get_teams)