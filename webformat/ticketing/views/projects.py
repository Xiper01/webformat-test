from ast import For
from hashlib import new
from multiprocessing import context
from pydoc import describe
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

from ticketing.models import Project, Task
from django.conf import settings
from django.contrib.auth import get_user_model
from ticketing.forms import FormCreateProject, FormCreateTask

# GET all projects
def get_projects(request):
  context = {"projects": Project.objects.all()}
  return render(request, "project/index.html", context)

# GET specific project
def get_project(request, pk):
  project = Project.objects.get(pk=pk)
  tasks = Task.objects.filter(project=project)
  task_form = FormCreateTask()
  developers = get_user_model().objects.filter(role = 3)
  context = {
    "task_form": task_form,
    "project": project,
    "tasks": tasks,
    "developers": developers,
    "logged_user": request.user
  }
  return render(request, "project/show.html", context)

# POST create task
def create_task(request, pk):
  form = request.POST
  print(form)
  project = Project.objects.get(pk=pk)
  new_task = Task(project=project, description=form.get("description"), status=form.get("status"), deadline=form.get("deadline"), creator=request.user)
  new_task.save()
  new_task.assignees.set(form.getlist("assignees"))
  return redirect(get_project, pk)

# POST delete task
def delete_task(request, pk):
  form = request.POST
  task = Task.objects.get(pk=form.get("task_id"))
  task.delete()
  return redirect(get_project, pk)

# GET create a new project
def new_project(request):
  if request.method == "POST":
    form = request.POST
    new_project = Project(name = form.get("name"), description = form.get("description"))
    new_project.save()
    new_project.members.set(form.getlist("developer"))
    return redirect(get_projects)
  else:
    form = FormCreateProject()
    pms = get_user_model().objects.filter(role = 2)
  context = {
    "form": form,
    "pms": pms
  }
  return render(request, "project/new.html", context)

# GET edit a project
def edit_project(request, pk):
  project = Project.objects.get(pk=pk)
  if request.method == "POST":
    form = request.POST
    project.name = form.get("name")
    project.description = form.get("description")
    project.save()
    project.members.set(form.getlist("developer"))
    return redirect(get_projects)
  else:
    form = FormCreateProject()
    pms = get_user_model().objects.filter(role = 2)
  context = {
    "form": form,
    "pms": pms,
    "project": project
  }
  return render(request, "project/edit.html", context)

# POST delete project
def delete_project(request, pk):
  project = Project.objects.get(pk=pk)
  project.delete()
  return redirect(get_projects)