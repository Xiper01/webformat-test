from ast import For
from hashlib import new
from multiprocessing import context
from pydoc import describe
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

from ticketing.models import Project, Task
from django.conf import settings
from django.contrib.auth import get_user_model
from ticketing.forms import FormCreateProject, FormCreateTask

# GET all employees
def get_employees(request):
  context = {"employees": get_user_model().objects.all()}
  return render(request, "employee/index.html", context)

# GET an employee
def get_employee(request, pk):
  employee = get_user_model().objects.get(pk=pk)
  in_progress_tasks = Task.objects.filter(status=2)
  context = {
    "employee": employee,
    "tasks": in_progress_tasks
  }
  return render(request, "employee/show.html", context)