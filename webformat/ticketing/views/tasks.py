from ast import For
from hashlib import new
from multiprocessing import context
from pydoc import describe
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required

from ticketing.models import Project, Task, Commit
from django.conf import settings
from django.contrib.auth import get_user_model
from . import get_projects
from ticketing.forms import FormCreateTask


# GET all tasks
def get_tasks(request):
  context = {"tasks": Task.objects.filter(assignees__in=[request.user])}
  return render(request, "task/index.html", context)

# POST update task
def update_task(request, pk):
  task = Task.objects.get(pk=pk)
  if request.method == "POST":
    form = request.POST
    print(form)
    task.description = form.get("description")
    task.status = form.get("status")
    # task.deadline = form.get("deadline")
    task.save()
    task.assignees.set(form.getlist("developer"))
    return redirect(get_projects)
  developers = get_user_model().objects.filter(role = 3)
  task_form = FormCreateTask()
  context = {
    "task": task,
    "task_form": task_form,
    "developers": developers,
    "logged_user": request.user
  }
  return render(request, "task/edit.html", context)

# GET get commits
def commits_task(request, pk):
  task = Task.objects.get(pk=pk)
  if request.method == "POST":
    form = request.POST
    commit = Commit(creator=request.user, note=form.get("comment"), task=task)
    commit.save()
  commits = task.commit_set.all()
  context = {
    "task": task,
    "commits": commits
  }
  return render(request, "task/show.html", context)