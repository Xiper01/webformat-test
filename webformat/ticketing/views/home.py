from ast import For
from hashlib import new
from multiprocessing import context
from pydoc import describe
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as django_logout

@login_required
def home(request):
  context = {
    "logged_user": request.user
  }
  return render(request, "homepage.html", context)

@login_required
def logout(request):
  django_logout(request)
  return redirect(home)