from django import forms
from .models import Team, Project, Task, Commit

class FormCreateTeam(forms.ModelForm):

  class Meta:
    model = Team
    fields = "__all__"
    exclude = ('members',)


class FormCreateProject(forms.ModelForm):

  class Meta:
    model = Project
    fields = "__all__"
    exclude = ('creator', 'pm')


class FormCreateTask(forms.ModelForm):

  class Meta:
    model = Task
    fields = "__all__"
    exclude = ('creator', 'assignees', 'project', 'deadline')


class FormCreateCommit(forms.ModelForm):

  class Meta:
    model = Commit
    fields = "__all__"
