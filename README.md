# Webformat Test

## Installazione

Creare un Python venv `python -m venv ./.venv`

Attivare il venv `source ./.venv/bin/activate`

(Per Windows) `./.venv/Script/activate`

Installare le librerie `pip install -r requirements.txt`

Entrare nella cartella di progetto `cd ./webformat`

Applicare il seed per il db `python manage.py dumpdata --format yaml -o ./seed.yaml`

Avviare il server `python manage.py startserver`

## Utilizzo

Per il login, sono stati creati 6 utenti di prova, tutti con password Vmware1!

- ceo
- pm_1
- pm_2
- dev_1
- dev_2
- dev_3

## Navigazione del progetto

Il progetto è suddiviso in 4 cartelle principali:

- webformat
- employees
- ticketing
- templates

### webformat

Console principale per la gestione del progetto. Al suo interno vi è la configurazione principale di progetto.

### employees

Utilizzando il modello già presente di user, e modificandolo ad hoc, è stata creata questa applicazione per dividere dal resto questa configurazione. L'import del nuovo tipo di user, come da documentazione, viene effettuata tramite una variabile dichiarata nel settings.py.

### ticketing

Cuore del progetto, al cui interno vi sono modelli, views, routes, forms, e tutto il necessario.

### templates

Cartella centralizzata per la creazione delle pagine web e dei template da visualizzare.

## Notazioni sulla soluzione

A causa di insufficienza di tempo, o di feature di minore importanza, vi è da notare:

1. Solo le route principali sono coperte da @login_required, ma idealmente quasi ogni chiamata dovrebbere essere bloccata per gli utenti non loggati.

2. Le operazioni consentite solo ad alcuni utenti sono completamente configurate bloccando la visualizzazione di elementi lato front, quando vi andrebbero dei controlli più rigidi lato back.

